publisher = {"name": "fedora", "host": "helsinki", "app_id": "fedora"}
message_broker = {"url": "amqp://guest:guest@rabbit-mq:5672/"}
exchange = {"name": "amq.topic", "exchange_type": "topic", "durable": "True"}
routing_keys = {"c8s_build": "org.centos.prod.ci.product-build.artifact"}
scraper = {
    "url": "https://koji.mbox.centos.org/koji/search?match=glob&type=package&terms=",  # noqa: E501
    "re": r".*buildinfo\?buildID=.*",
    "success_object": {"class": "complete"},
}
message = {
    "header": {
        "uri": "amqp://guest:guest@rabbit-mq:5672/",
    },
    "body": {
        "artifact": {
            "scratch": "false",
            "id": "68771870",
            "type": "koji-build",
            "issuer": "scrapper",
        }
    },
    "message_id": "random",
    "content_encoding": "utf-8",
}
