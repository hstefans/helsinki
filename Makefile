.PHONY: dependencies unit lint

dependencies:
	pip install --upgrade pip
	pip install -r requirements.txt
	pip install -r tests/requirements.txt


unit: dependencies
	PYTHONPATH="$PYTHONPATH:./helsinki" pytest --cov=helsinki --cov-report \
		term-missing --cov-fail-under=0 tests/unit

lint: dependencies
	pre-commit run --all-files
